package com.example.egs.service.impl;

import com.example.egs.dto.request.UserRequest;
import com.example.egs.dto.response.UserResponse;
import com.example.egs.entity.User;
import com.example.egs.mapper.UserMapper;
import com.example.egs.repository.UserRepository;
import com.example.egs.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    private final ModelMapper modelMapper;

    @Override
    public void saveUser(UserRequest userRequest) {
        userRepository.save(userMapper.toEntity(userRequest));
    }

    @Override
    public UserResponse findUserById(Long id) {
        Optional<User> userById = userRepository.findById(id);
        if (userById.isPresent()) {
            return userMapper.toResponse(userById.get());
        }
        throw new EntityNotFoundException("that user isn`t in DB");
    }

    @Override
    public List<UserResponse> findAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(userMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public UserResponse updateUser(Long id, UserRequest userRequest) {

        User user = userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        modelMapper.map(userRequest, user);
        return userMapper.toResponse(user);
    }

    @Override
    @Transactional
    public UserResponse updateUserByField(Long id, String name) {
        User user = userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        user.setName(name);
        return userMapper.toResponse(user);
    }

    @Override
    public void deleteUserById(Long id) {
        Optional<User> userById = userRepository.findById(id);
        if (userById.isPresent()) {
            userRepository.deleteById(id);
        }
    }
}
