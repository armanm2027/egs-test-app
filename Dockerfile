FROM openjdk:11
COPY target/user-0.0.1.jar user.jar

CMD ["java", "-jar", "user.jar"]
