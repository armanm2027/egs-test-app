<h1 align="center">EGS Test Application.</h1>

<hr>

## Documentation

Project has built with

- Spring boot,Data,Rest
- Architecture type - monolith
- Deployment Docker
- Database Postgres
- build maven

## Development Setup

### Prerequisites

- Install java 11

### Setting Up a Project

## Stage 1 run locally

Install the Postgres 13:
Every project has own DB server but when you need to run projects locally you can create databases on one server

Create databases:
You need to create databases for every running application( if need ) because they will not create their databases
automatically

Run the application:
You don't need to do anything for successfully run

## Stage 2 run into docker

Install docker

run docker compose script from root directory

```
docker-compose up -d
```

You need only run compose script

## Swagger

Swagger url is http://hostname:port/swagger-ui/ <br>

For example http://localhost:8080/swagger-ui/