package com.example.egs.mapper;

import com.example.egs.dto.request.UserRequest;
import com.example.egs.dto.response.UserResponse;
import com.example.egs.entity.User;
import com.example.egs.mapper.config.BaseMapper;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserMapper implements BaseMapper<User, UserRequest, UserResponse> {

    private final ModelMapper mapper;

    @Override
    public User toEntity(UserRequest userRequest) {
        return mapper.map(userRequest, User.class);
    }

    @Override
    public UserResponse toResponse(User user) {
        return mapper.map(user, UserResponse.class);
    }
}
