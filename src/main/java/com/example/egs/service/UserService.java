package com.example.egs.service;

import com.example.egs.dto.request.UserRequest;
import com.example.egs.dto.response.UserResponse;

import java.util.List;

public interface UserService {

    void saveUser(UserRequest userRequest);

    UserResponse findUserById(Long id);

    List<UserResponse> findAllUsers();

    UserResponse updateUser(Long id, UserRequest userRequest);

    UserResponse updateUserByField(Long id, String field);

    void deleteUserById(Long id);


}
