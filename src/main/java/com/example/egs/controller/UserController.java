package com.example.egs.controller;

import com.example.egs.dto.request.UserRequest;
import com.example.egs.dto.response.UserResponse;
import com.example.egs.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;


    @ApiOperation(value = "save user")
    @PostMapping("/")
    public ResponseEntity<?> saveUser(@Valid @RequestBody UserRequest request) {
        userService.saveUser(request);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * @param id This controller find user byId, when id exist return user from DB
     */
    @ApiOperation(value = "find user by id")
    @GetMapping("/{id}")
    public ResponseEntity<?> findUserById(@PathVariable("id") long id) {
        return ResponseEntity.ok(userService.findUserById(id));
    }


    /**
     * @return ResponseEntity<List < UserResponse>> This controller find all users from DB
     */
    @ApiOperation(value = "find all users")
    @GetMapping("/")
    public ResponseEntity<List<UserResponse>> findAll() {
        List<UserResponse> allUsers = userService.findAllUsers();
        return ResponseEntity.ok().body(allUsers);
    }

    /**
     * This method update users data
     * @param id
     * @param userRequest
     * @return ResponseEntity<UserResponse>
     */
    @ApiOperation(value = "change user data")
    @PutMapping("/{id}")
    public ResponseEntity<UserResponse> updateUser(@PathVariable("id") Long id,
                                                   @RequestBody UserRequest userRequest) {
        return ResponseEntity.ok().body(userService.updateUser(id, userRequest));
    }

    /**
     * This method change user name
     * @param id
     * @param name
     * @return ResponseEntity<UserResponse>
     */
    @ApiOperation(value = "change user  name")
    @PatchMapping("/{id}")
    public ResponseEntity<UserResponse> updateUserByField(@PathVariable("id") Long id,
                                                   @RequestParam String name) {
        return ResponseEntity.ok(userService.updateUserByField(id, name));
    }

    /**
     * This method delete user  by id
     * @param id
     * @return ResponseEntity<HttpStatus>
     */
    @ApiOperation("delete user By id")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable("id") Long id){
        userService.deleteUserById(id);
        return  ResponseEntity.ok().build();
    }
}
